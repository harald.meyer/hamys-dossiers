-- drop an old version of the table 
drop table if exists composers;

-- create the table and populate all columns
-- with the exception of the image column
-- from a CSV table.
create table composers(
    id int primary key,
    initials varchar(8) unique,
    first_name varchar(60) not null,
    last_name varchar(60) not null,
    born date not null,
    died date not null,
    image blob
) as
select
    id,
    initials,
    first_name,
    last_name,
    born,
    died,
    null
from
    csvread('../common/data/composers/composers.csv') with data;

-- Last, but not least, we read the missing image BLOB
-- data from the file system.
update
    composers
set
    image = file_read(
        '../common/data/composers/wikipedia-images/' || lower(initials) || '.jpg'
    )
where
    initials in (
        select
            initials
        from
            composers
    );