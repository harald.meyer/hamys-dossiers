import java.sql.*;
import javax.swing.*;
import static javax.swing.SwingConstants.*;

Class.forName("org.h2.Driver");

void say(Object ... args) {
    for (var arg:args) {System.out.print(arg);}
    System.out.println();
}

var url = "jdbc:h2:mem:;init=runscript from '../common/data/composers/composers.sql'";
var conn = DriverManager.getConnection (url, "sa",""); 
say("conn: ", conn);
var stmt = conn.createStatement(); 
say("stmt: ",stmt);

JFrame frame = new JFrame("Quick & Dirty JShell Client");
frame.setSize(700,700);
JLabel label = new JLabel("");
label.setVerticalTextPosition(BOTTOM);
label.setHorizontalTextPosition(CENTER);
label.setVerticalAlignment(CENTER);
label.setHorizontalAlignment(CENTER);
frame.add(label);
frame.setVisible(true);

var rs  = stmt.executeQuery("select * from composers");
say("rs: ",rs);
while (rs.next()) {
    var id = rs.getInt(1);
    var initials = rs.getString(2);
    var firstName = rs.getString(3);
    var lastName = rs.getString(4);
    var born = rs.getDate(5);
    var died = rs.getDate(6);
    var blob = rs.getBytes(7);
    say(id," ",initials," ",firstName," ",lastName," ",born," ",died," ",blob.length);
    label.setText("<html>" + firstName + " " + lastName + "<br>(*) " + born + "<br>(+) " + died + "</html>");
    label.setIcon(new ImageIcon(blob)); 
    Thread.sleep(500);
}

frame.setVisible(false);
conn.close(); 
/exit