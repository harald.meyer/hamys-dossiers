os = require('os');

console.log("***** OS functions *****");
console.log("os.arch(): ",os.arch());
console.log("os.availableParallelism(): ",os.availableParallelism());
console.log("os.cpus(): ",os.cpus());
console.log("os.endianness(): ",os.endianness());
console.log("os.freemem(): ",os.freemem());
console.log("os.getPriority(): ",os.getPriority());
console.log("os.homedir(): ",os.homedir());
console.log("os.hostname(): ",os.hostname());
console.log("os.loadavg() 1/5/15 min avg: ",os.loadavg());
console.log("os.networkInterfaces(): ",os.networkInterfaces());
console.log("os.platform(): ",os.platform());
console.log("os.release(): ",os.release());
console.log("os.tmpdir(): ",os.tmpdir());
console.log("os.totalmem(): ",os.totalmem());
console.log("os.type(): ",os.type());
console.log("os.userInfo(): ",os.userInfo());
console.log("os.uptime(): ",os.uptime()," secs = ",os.uptime()/86400.0," days");
console.log("os.version(): ",os.version());
console.log("os.machine(): ",os.machine());
console.log("");
console.log("***** __ variables *****");
console.log("__dirname: ",__dirname);
console.log("__filename: ",__filename);

