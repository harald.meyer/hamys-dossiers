#!/bin/bash

docker build --tag hamy-base-node .
docker rm -f hamy-base-node
docker run --name hamy-base-node --tty --interactive hamy-base-node
